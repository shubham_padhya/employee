/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package employee;

import java.util.Date;

/**
 *
 * @author SHUBHAM PADHYA
 */
public class Employee {
    private String employeeId;
    private Date dateOfJoining;
    private double salary;
    private String name ;

    public Employee(String employeeId, Date dateJoining, double salary) {
        this.employeeId = employeeId;
        this.dateOfJoining = dateJoining;
        this.salary = salary;
    }
    
    
    public String getEmployeeId(){
        return employeeId;
    }

    public Date getEmployeeDateOfJoining() {
        return dateOfJoining;
    }

    public double getEmployeeSalary() {
        return salary;
    }

    public String getEmployeeName() {
        return name;
    }
    
    
}
   
